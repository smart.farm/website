<?php
define('ROOT', dirname(__FILE__).'\\');
include "Resources/Private/Data.php"
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>SmartFarm | <?php echo $GLOBALS['currentPage']['title'] ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $GLOBALS['baseUrl']; ?>favicon.ico">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open Sans:300,400,600,700|Muli:300,300italic,400,400italic,600,600italic,700,700italic,900,900italic|Playfair Display:400,400italic,700,700italic&subset=latin,latin-ext" rel="stylesheet">
    <?php
     echo '<link rel="Stylesheet" href="'.$GLOBALS['webRoot'].'Resources/Public/Css/main.css"/>';
	 echo '<link rel="Stylesheet" href="'.$GLOBALS['webRoot'].'Resources/Public/Css/test.css"/>';
	?>
</head>
<body>

<?php
echo '<script src="'.$GLOBALS['publicPath'].'Javascripts/Vendor/TweenMax.min.js"></script>';
echo '<script src="'.$GLOBALS['publicPath'].'Javascripts/Vendor/jquery.min.js"></script>';
echo '<script src="'.$GLOBALS['publicPath'].'Javascripts/Vendor/bootstrap.bundle.min.js"></script>';
echo '<script src="'.$GLOBALS['publicPath'].'Javascripts/Vendor/slick.min.js"></script>';
echo '<script src="'.$GLOBALS['publicPath'].'Javascripts/Vendor/aos.js"></script>';
echo '<script src="'.$GLOBALS['publicPath'].'Javascripts/ShoppingCart.js"></script>';
echo '<script src="'.$GLOBALS['publicPath'].'Javascripts/Main.js"></script>';
?>

<?php include 'Resources/Private/Template/Main.php'; ?>

</body>
</html>