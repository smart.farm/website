/**
 * Binds all ShoppingCart functions to window.ShoppingCart
 */
var ShoppingCart = {

    keyItems: 'items',
    items: [],
    // an item for example: {id: 0, count: 1}
    idMain: -2,

    /**
     * Init function. Loads Items from storage, updates view and add button listeners
     */
    init: function( ) {
        ShoppingCart.items = this.getItems();
        this.updateView();
        this.registerAddToCartButtons();
        this.registerRemoveFromCartButtons();
    },

    /**
     * Set whole item array.
     * After each setItem we will update the view
     * @param {array} items the new item array to set
     */
    setItems: function( items ) {
        ShoppingCart.items = items;
        localStorage.setItem(this.keyItems, JSON.stringify(items));

        // Update View
        this.updateView();
    },

    /**
     * Loading Items from storage and check if content is valid.
     * @returns {*} empty array if no valid data found, else array with data
     */
    getItems: function() {
        var storage =  localStorage.getItem(this.keyItems);
        if(storage !== null && typeof storage !== 'undefined') {
            try {
                var obj = JSON.parse(storage);
                if(Array.isArray(obj)) {
                    return obj;
                }
            } catch (e) {
                console.warn("Failed loading the Shoppingcart. Resetting.");
                console.warn(e);
                return [];
            }
        }
        return [];
    },

    /**
     * Adds a single item to ShoppingCart List. Item gets replaced if there is already the same id in storage
     * @param {*} item the item to add
     */
    addItem: function( item ) {
        var storage = this.getItems();
        var changed = false;

        for ( var i = 0; i < storage.length; i++) {
            var storeItem = storage[i];
            if (storeItem.id === item.id) {
                storage[i] = item;
                changed = true;
            }
        }

        if (!changed) {
            storage.push(item);
        }

        this.setItems(storage);
    },

    /**
     * Remove a single item from cart based on its id
     * @param id the id to remove
     */
    removeItem: function( id ) {
        var filtered = ShoppingCart.items.filter(function(value, index, arr){
            return value.id !== id;
        });
        this.setItems(filtered);
    },

    /**
     * Updates the badge in the top navigation
     */
    updateView: function() {
        var badge = $('.shopping-badge');
        if(ShoppingCart.items.length > 0) {
            badge.show();
            badge.html(ShoppingCart.items.length);
        } else {
            badge.hide();
        }
    },

    /**
     * Add listeners for "Add to cart"-Buttons
     * Buttons are identified by having attribute sc-id="<id>"
     */
    registerAddToCartButtons: function() {
        $('[sc-id]').each( function() {
            $(this).on('click', function() {

                var el = $(this);
                var count = el.attr('sc-count');
                if( typeof count === "undefined") {
                    count = 1;
                }

                ShoppingCart.addItem({
                    id: el.attr('sc-id'),
                    count: count
                })
            })
        });

        $('[sc-main]').each( function() {
            $(this).on('click', function() {
                ShoppingCart.addItem({
                    id: ShoppingCart.idMain
                })
            })
        });
    },

    /**
     * Add listeners for "remove from cart"-Buttons
     * Remove button are identified by attribute sc-remove="<id>"
     */
    registerRemoveFromCartButtons: function() {
        $('[sc-remove]').each( function() {
            $(this).on('click', function() {

                var el = $(this);
                var id = el.attr('sc-remove');
                if( typeof count === "undefined") {
                    console.error("ID not found");
                }

                ShoppingCart.removeItem(id);
                window.location.reload();
            })
        });
    },

    removeItems: function () {
        this.setItems([]);
    }

};

(function() {
    $(document).ready(function() {
        /**
         * Initializes the ShoppingCard lib
         */
        ShoppingCart.init();
    });
}());