/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 17.04.2019
 * The Main JS. Binds all the main functions to window.SmartFarm to make it globally callable.
 */


var SmartFarm = {

    /**
     * Initializes the homepage header.
     * The Navigation is initially transparent.
     * Changed the background color of nav to black when scrolling down.
     */
    initHeader: function(){
        $(window).scroll(function(event) {
            var doc = document.documentElement;
            var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
            var navbar = $('.navbar');

            var trigger = $('#header').height() - navbar.outerHeight();

            if(top > trigger) {
                navbar.css('background-color','black');
            } else {
                navbar.css('background-color', '');
            }
        })
    },

    /**
     * Prevents Responsive issues when scaling window.
     * Adding fixed top light navbar for mobile view
     */
    handleResize: function() {
        var navbar = $('.navbar');
        if($(window).width() < 975) {
            navbar.removeClass("fixed-top navbar-dark bg-dark");
            navbar.addClass("navbar-light");
        } else {
            navbar.removeClass("navbar-light");
            if (!navbar.hasClass("fixed-top") && !navbar.hasClass("not-fixed"))
                navbar.addClass("fixed-top");
            if(!navbar.hasClass("navbar-dark"))
                navbar.addClass("navbar-dark");
            if(!navbar.hasClass("trans"))
                navbar.addClass("bg-dark");
        }
    },

    /**
     * Indicates whether the video is playing or not
     */
    bgIsPlaying: false,

    /**
     * Checks if the video element is visible when scrolling.
     * Start playing when scrolling into view, stops when scrolling out.
     */
    autoPlayBackgroundVideo: function() {
        $('.bg-movie').each(function (value) {
            var $elem = $(this);
            var $wrapper = $elem.parents(".bg-movie-wrapper");

            if(SmartFarm.util.isScrolledIntoView($wrapper)) {
                if(!SmartFarm.bgIsPlaying) {
                    try {
                        this.play();
                    } catch (e) {
                        console.warn(e);
                    }
                }
                SmartFarm.bgIsPlaying = true;
            } else {
                this.pause();
                SmartFarm.bgIsPlaying = false;
            }
        });
    },

    util: {
        /**
         * Function to check if Element is scrolled into view
         * @param elem the corresponding element
         * @returns {boolean} whether its visible or not
         */
        isScrolledIntoView: function(elem) {
            var docViewTop = $(window).scrollTop();
            var docViewBottom = docViewTop + window.innerHeight;
            var elemTop = $(elem).offset().top;
            var elemBottom = elemTop + $(elem).height();
            return ((elemTop <= docViewBottom) && (elemBottom >= docViewTop));
        },

        /**
         * Adds thousands separator to large number.
         * see https://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
         * @param x the number to add separator
         * @returns {string} the number with separators
         */
        numberWithCommas: function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
    },

    /*
     * Replace all SVG images with inline SVG
     */
    initSVGReplace: function() {
        $('img[src$=".svg"]:not(.noreplace)').each(function(){
            var $img = $(this);
            var imgURL = $img.attr('src');
            var imgClass = $img.attr('class');

            $.get(imgURL, function(data) {
                // Get the SVG tag, ignore the rest
                var $svg = $(data).find('svg');

                // Add replaced image's classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml');

        });
    },

    /**
     * Calls the Login service
     * @param action the url to call
     * @param user the username to pass
     * @param pass the password to pass
     * @param callback
     */
    login: function(action, user, pass, callback) {
        action += '?user='+user+'&pass='+pass;
        return $.get(action, function( data ) {
            callback(JSON.parse(data));
        });
    },

    /**
     * Checks if cookie note should be displayed
     */
    checkCookies: function() {
        if(localStorage.getItem('hideCookieNote') === "1") {
            $('.cookie-wrapper').remove();
        }
    },

    /**
     * Removes cookie note
     */
    enableCookie: function() {
        localStorage.setItem('hideCookieNote', "1");
        $('.cookie-wrapper').remove();
    }
};

$(document).ready(function() {

    /**
     * Initializes default slick slider
     */
    $('.slick-slider').slick({
        autoplay: false,
        autoplaySpeed: 3000,
        fade: true,
        infinite: true,
        arrows: false
    });

    /**
     * Call init functions of main lib
     */
    SmartFarm.initHeader();
    SmartFarm.handleResize();
    SmartFarm.autoPlayBackgroundVideo();
    SmartFarm.checkCookies();

    $('#enable-cookie').click(function() {
        SmartFarm.enableCookie();
    });

    $(window).resize(function() {
        SmartFarm.handleResize();
    });

    $(window).scroll(function(event) {
        SmartFarm.autoPlayBackgroundVideo();
    });

    /**
     * Initialize slider for cooperations in footer
     */
    $('.partner-slider').slick({
        autoplay: true,
        slidesToShow: 4,
        autoplaySpeed: 3000,
        infinite: true,
    });

    /**
     *  Init AOS Animation library
     */
    AOS.init();

    /**
     * Disable sending empty search requests
     */
    $('#search-form [type=submit]').on('click', function( event ) {
        if($('#search-form [name=q]').val().length < 1) event.preventDefault();
    });

    $('form.login').each(function() {
        $(this).submit( function(e) {
            var $form = $(this);
            SmartFarm.login($form.attr('call'),$form.find('[name=user]').val(),$form.find('[name=pass]').val(), function( data ) {
                if(data.success) {
                    $('#modal-login .data.name').html(data.data.name);
                    $('#modal-login').modal();
                }
            });
            e.preventDefault();
        })
    })
});