(function() {
    window.slider ={};
    window.slider.hasBasePackage = false;
    //lets display the static output now
    window.updateOutput = function(figure, activate) {
        //if activate then add .active to #input-wrapper to help animate the #reel
        if(activate)
            $("#input-wrapper").addClass("active");

        //because of the step param the slider will return values in multiple of 0.05 so we have to round it up
        rfigure = Math.round(figure);
        //displaying the static output
        $("#static-output").html(rfigure);

        //positioning #static-output and #reel
        //horizontal positioning first
        h = figure/window.slider.max*($("#input-wrapper").width()-$("#reel").width()) + 'px';
        //vertical positioning of #rn
        v = rfigure*$("#reel").height()*-1 + 'px';

        //applying the positions
        $("#static-output, #reel").css({left: h});
        //#rn will be moved using transform+transitions for better animation performance. The false translateZ triggers GPU acceleration for better performance.
        $("#rn").css({transform: 'translateY('+v+') translateZ(0)'});

        var price = 0;

        if(rfigure < 10) {
            price = PRICES.multi['1 - 10'];
        } else if(rfigure < 50) {
            price = PRICES.multi['10 - 50'];
        } else {
            price = PRICES.multi['ab 50'];
        }

        price = parseFloat(price.replace('.',''));

        $('#price-hecatre').html(price.toLocaleString());

        $('#calc-price').html((rfigure * price).toLocaleString());
        $('.btn.buy').attr('sc-count',rfigure);
    };

    window.deactivate = function() {
        //remove .active from #input-wrapper
        $("#input-wrapper").removeClass("active");
    };

    $(document).ready(function() {
        //lets populate reel numbers
        var slider = $("#rangeslider");
        window.slider.min = slider.attr("min");
        window.slider.max = slider.attr("max");

        var rn = "";
        for (var i = window.slider.min; i <= window.slider.max; i++) {
            var count = i;
            if( i == window.slider.max) {
                count = '>'+i;
            }
            rn += "<span>" + count + "</span>";
        }
        $("#rn").html(rn);
        $("#reel").css("width",((""+window.slider.max).length + 1 )+'rem');

//triggering updateOutput manually
        window.updateOutput(slider.val(), false);

        window.slider.rfigure = 0;
        window.slider.h = 0;
        window.slider.v = 0;

        if(msieversion()) {
            $('.section.pricecalculator').remove();
        }
    });
}());


// @see https://stackoverflow.com/questions/19999388/check-if-user-is-using-ie
function msieversion() {
    return (navigator.appName == 'Microsoft Internet Explorer' ||
        !!(navigator.userAgent.match(/Trident/) ||
            navigator.userAgent.match(/rv:11/)) ||
        (typeof $.browser !== "undefined" && $.browser.msie == 1));
}