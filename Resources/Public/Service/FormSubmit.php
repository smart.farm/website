<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 07.05.2019
 * This Submit Handler just calls our Formhandler class to validate content and execute further actions.
 */

use SmartFarm\Web\Classes\Auth;
use SmartFarm\Web\Classes\FormHandler;
define('ROOT', dirname(__FILE__).'\\..\\..\\..\\..\\');
include '../../Private/Data.php';

$data = FormHandler::processRequest();
echo json_encode($data);