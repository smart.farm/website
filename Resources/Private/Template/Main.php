<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 26.03.2019
 * The Main Included file. This File handles the whole body part of out HTML.
 * It first renders the navigation, checks if we need a header and renders the previously selected page content.
 * After Content, it renders the footer file.
 */

// Layout
include $GLOBALS["partialRootPath"].'Nav.php';

if ($GLOBALS['currentPage']['config']['showHeader']) {
    $headerTemplate = $GLOBALS['currentPage']['path'].'Header.php';
    include (file_exists($headerTemplate) ? $headerTemplate :  $GLOBALS["partialRootPath"].'Header.php');
}

include ROOT . $GLOBALS['currentPage']['path'].'Content.php';

include $GLOBALS["partialRootPath"].'Footer.php';