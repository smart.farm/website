<?php

/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 22.04.2019
 * A Class for logging from php directly in js console. This prevents User to see the message.
 */
namespace SmartFarm\Web\Classes;

class Log {

    /**
     * Logs a message in JS console, so its not visible to the frontend
     * @param $msg {string} the message to print in the console output
     */
    static function js($msg) {
        echo '<script type="text/javascript">console.log("[PHP] '.$msg.'")</script>';
    }
}