<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 07.05.2019
 * Time: 08:32
 */

namespace SmartFarm\Web\Classes;


use Exception;

class Auth
{

    // Hehe we are so good
    static $logins = [
        'jstorm@smart-farm.de' => [
            'hash' => '2cac6e041958e88e62a619a7df517ff4',
            'data' => [
                'name' => 'Jan Storm',
            ]
        ],
        'mcroon@smart-farm.de' => [
            'hash' => '2cac6e041958e88e62a619a7df517ff4',
            'data' => [
                'name' => 'Max Croon',
            ]
        ],
        'fkupfer@smart-farm.de' => [
            'hash' => '2cac6e041958e88e62a619a7df517ff4',
            'data' => [
                'name' => 'Friedemann Kupfer',
            ]
        ]
    ];

    public static function login($user, $pass)
    {
        $hash = md5($pass);
        if(!isset(self::$logins[$user]) || !isset(self::$logins[$user]['hash'])) {
            return ['success' => false];
        }

        if (self::$logins[$user]['hash'] == $hash) {
            return [
                'success' => true,
                'data' => self::$logins[$user]['data']
            ];
        }
        return ['success' => false];
    }
}