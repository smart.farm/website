<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 07.05.2019
 */

namespace SmartFarm\Web\Classes;


use function Sodium\add;

/**
 * Class Search - Searches through the data from products and pages based on GLOBALS
 * First searches For app occurrences and then for page title and content.php files.
 * Titles are higher ranked than occurrence in Content.php
 */
class Search
{
    protected $input = '';
    protected $keywords = [];
    protected $result = [];

    /**
     * Search constructor.
     * @param $input
     */
    public function __construct($input)
    {
        $this->input = strtolower($input);
    }

    /**
     * Splices the whole search request by space into a set of keywords.
     * Executes the main search, compress results and sort the results by rank
     */
    function execute() {
        $this->keywords = explode(' ', $this->input);
        for ($i = 0; $i < sizeof($this->keywords); $i++) {
            $keyword = $this->keywords[$i];
            $this->searchForKeyword($keyword);
        }
        $this->compressResult();
        $this->sortResultByRank();
    }

    /**
     * Search for a single Keyword first in App, then in pages
     * @param $keyword string the keyword to search for
     */
    function searchForKeyword( $keyword ){

        foreach ($GLOBALS['apps'] as $key => $app) {
          $this->searchApp($app, $keyword);
        }
        foreach ($GLOBALS['pageMapping'] as $key => $page) {
          $this->searchPage($page, $keyword);
        }
    }

    /**
     * Search in Apps. Searched fields and ranks are title (10), teaser (7), description (3),  includes (5), Author name (6)
     * @param $app array the app data to search
     * @param $keyword string the keyword to match
     */
    private function searchApp( $app , $keyword) {
        if($this->contains($app['title'], $keyword)) {
            $this->addResult(
                'product/'.$app['title'],
                $GLOBALS['pageMapping'][$GLOBALS['productPage']],
                10,
                $this->count($app['title'], $keyword),
                $app);
        }
        if($this->contains($app['teaser'], $keyword)) {
            $this->addResult(
                'product/'.$app['title'],
                $GLOBALS['pageMapping'][$GLOBALS['productPage']],
                7,
                $this->count($app['teaser'], $keyword),
                $app);
        }
        if($this->contains($app['description'], $keyword)) {
            $this->addResult(
                'product/'.$app['title'],
                $GLOBALS['pageMapping'][$GLOBALS['productPage']],
                3,
                $this->count($app['description'], $keyword),
                $app);
        }
        foreach ($app['includes'] as $key => $include) {
            if($this->contains($include, $keyword)) {
                $this->addResult(
                    'product/'.$app['title'],
                    $GLOBALS['pageMapping'][$GLOBALS['productPage']],
                    5,
                    $this->count($include, $keyword),
                    $app);
            }
        }
        if($this->contains($app['author']['name'], $keyword)) {
            $this->addResult(
                'product/'.$app['title'],
                $GLOBALS['pageMapping'][$GLOBALS['productPage']],
                6,
                $this->count($app['author']['name'], $keyword),
                $app);
        }
    }

    /**
     * Searches a Page. Searched fields and ranks are title (10), altTitle (9), Content.php (1 per Occurence)
     * @param $page array the page data to search
     * @param $keyword string the keyword to match
     */
    private function searchPage($page , $keyword) {
        if(!$page['config']['searchable']) {
            // Skip page
            return;
        }

        if($this->contains($page['title'], $keyword)) {
            $this->addResult($page['url'], $page, 10, $this->count($page['title'], $keyword));
        }
        if(isset($page['altTitle']) && $this->contains($page['altTitle'], $keyword)) {
            $this->addResult($page['url'], $page, 9, $this->count($page['altTitle'], $keyword));
        }

        $content = file_get_contents(ROOT . $page['path'] . 'Content.php');

        if(isset($content) && $this->contains($content, $keyword)) {
            $this->addResult($page['url'], $page, $this->count($content, $keyword), $this->count($content, $keyword));
        }
    }

    /**
     * In the previous functions, we just added every little occurence found. In this function we merge all of that by page.
     * So the Occurences and the rank is based on page. (Occurences and Ranks are getting summed up)
     */
    function compressResult() {
        $compResult = [];
        foreach ($this->result as $key => $item) {

            //Check if we already have this link
            $found = false;
            for($i = 0; $i < sizeof($compResult); $i++) {
                if($compResult[$i]['link'] === $item['link']) {
                    $compResult[$i]['rank'] += $item['rank'];
                    $compResult[$i]['occurences'] += $item['occurences'];
                    $found = true;
                }
            }

            if(!$found) {
                array_push($compResult, $item);
            }
        }
        $this->result = $compResult;
    }

    /**
     * Sorts the whole result object array by rank
     * @see https://stackoverflow.com/questions/11312986/usort-function-in-a-class
     */
    function sortResultByRank() {
        usort($this->result, array($this, "cmp"));
    }

    /**
     * Checks if substring exists in string. Cases are ignored.
     * @param $haystack string the String to search in
     * @param $needle string the substring to search for
     * @return bool whether string contains substring
     */
    private function contains($haystack, $needle) {
        return strpos(strtolower($haystack), strtolower($needle)) !== false;
    }

    /**
     * Counts occurrences of substring in string.r
     * @param $haystack string the String to search in
     * @param $needle string the substring to search for
     * @return int the count of occurrences
     */
    private function count($haystack, $needle) {
        return substr_count(strtolower($haystack), strtolower($needle));
    }

    /**
     * Checks if a value is lower than the other. Used for sorting in $this->sortResultByRank()
     * @param $a array the first object to check
     * @param $b array the Second object to check
     * @return bool if a is smaller than b
     */
    function cmp($a, $b){
        return $a['rank'] < $b['rank'];
    }

    /**
     * Add Item to result object array
     * @param $link string The link for the list view to refer to
     * @param $page array the page object from Globals
     * @param $rank int between 1 and 10 to show how relevant the entry is
     * @param $occurrences int the count of occurrences
     * @param $app array The App object from Globals
     */
    private function addResult($link, $page, $rank, $occurrences, $app = []) {
        array_push($this->result,[
            'link' => $link,
            'page' => $page,
            'rank' => $rank,
            'occurences' => $occurrences,
            'app' => $app,
        ]);
    }

    // Getter for Result
    function getResult() {
        return $this->result;
    }
}