<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 07.05.2019
 * Time: 00:13
 */
namespace SmartFarm\Web\Classes;

/**
 * Class FormHandler
 */
class FormHandler
{

    /**
     * Processes the Form submission request
     */
    public static function processRequest() {

        $login = FormHandler::validateUserData();
        if ($login['success']) {
            $GLOBALS['user'] = $login['data'];
        }

        return $login;
    }

    /**
     * Validates the data send from the form.
     * This is a second check, after the JS, because the js can be modified client-side.
     * @return array with user data
     */
    private static function validateUserData() {
        return Auth::login($_REQUEST['user'], $_REQUEST['pass']);
    }
}