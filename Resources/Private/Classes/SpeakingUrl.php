<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 26.03.2019
 * The SpeakingUrl Class. Here we select the content to render based on the current path given.
 * The Configuration of this can be found in Data.php => $GLOBALS['pages']
 */
namespace SmartFarm\Web\Classes;

class SpeakingUrl
{

    private $path = '';

    function __construct($path)
    {
        $this->path = trim($path, '/');
    }

    /**
     * This function interprets the current path and set its content based as it.
     * First, it checks the first level argument and checks if we have a config for it in $GLOBALS["pageMapping"]
     */
    function executeMapping()
    {
        // path indexes
        $path_title = 0;
        $show_404 = false;

        foreach ($GLOBALS['pageMapping'] as $page) {
            if (strpos(strtolower($this->path), strtolower($page['url'])) === 0) {
                $show_404 = false;
                $GLOBALS['currentPage'] = $page;
                $rest = trim(substr($this->path, strlen($page['url'])),'/'); // get everything beyond current url
                if(isset($page['param'])) { // if we have a param
                    foreach ($GLOBALS[$page['param']['global']] as $item) { // like apps
                        if (strtolower($item[$page['param']['check']]) == strtolower($rest)) { // check value
                            $GLOBALS[$page['param']['save']] = $item; //save item in global
                        }
                    }
                } else if(strlen($rest) > 0){
                    $show_404 = true;
                }
            }
        }
        if ($show_404) {
            $this->show_404();
        }
        if (!isset($GLOBALS['currentPage'])) {
            if ($this->path == "") {
                $this->show_home();
            } else {
                $this->show_404();
            }
        }
    }

    function show_404()
    {
        $GLOBALS['currentPage'] = $GLOBALS['pageMapping'][$GLOBALS["errorPage"]];
        http_response_code(404);
        header('Location: '.$GLOBALS['webRoot'].$GLOBALS['pageMapping'][$GLOBALS["errorPage"]]['url']);
    }

    function show_home()
    {
        $GLOBALS['currentPage'] = $GLOBALS['pageMapping'][$GLOBALS["startPage"]];
    }
}