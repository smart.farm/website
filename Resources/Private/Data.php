<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 02.04.2019
 * The Data Storage. Here we persist all the data for this website.
 */

// Classmap
include 'Classes/Log.php';
include 'Classes/SpeakingUrl.php';
include 'Classes/FormHandler.php';
include 'Classes/Auth.php';
include 'Classes/Search.php';
// Classmap ends

use SmartFarm\Web\Classes\SpeakingUrl;

// Vars•
$GLOBALS["baseUrl"] = 'http://localhost/smart-farm/';
$GLOBALS["webRoot"] = "/smart-farm/";
$GLOBALS["publicPath"] = $GLOBALS["webRoot"] . 'Resources/Public/';
$GLOBALS["iconPath"] = $GLOBALS["publicPath"] . 'Icons/';
$GLOBALS["imagePath"] = $GLOBALS["publicPath"] . 'Images/';
$GLOBALS['apps'] = [
    [
        'id' => '0',
        'title' => 'SmartWater',
        'icon' => 'Products/water.png',
        'image' => $GLOBALS["imagePath"] . 'Shop/Screenshot_1.png',
        'showcase' => $GLOBALS['webRoot'] . 'Resources/Public/Images/Produkt/FieldMate_png_nieuw_versie_5.png',
        'teaser' => 'Machen Sie ihre Bewässerungsanlage Smart! Mit diesem Modul sparen Sie sich lästiges Pflanzen gießen.',
        'description' => '<h2>MAXIMAL EFFIZIENT</h2><p>Die automatische Bewässerung sorgt dafür, dass Sie sich nie wieder um ihren Anbau sorgen müssen. Das automatisierte analysieren und reagieren regelt verlässlich die Bewässerung ihrer Pflanzen</p>
        <h2>DYNAMISCH ANPASSBAR</h2><p>Die Software erkennt automatisch, um welche Art von Anbau es sich handelt und versorgt jede Pflanze damit individuell. Sämtliche Einstellungen dazu lassen sich natürlich in der zentralen Steuerungsoftware anpassen.</p>
        <h2>VOLLKOMMEN AUTARK</h2><p>Das SmartWater Modul arbeitet vollkommen autark und Bedarf nach der Initialisierung durch unser Team keiner weiteren Arbeit. Dennoch lässt Sie sich natürlich auf Wunsch an der Zentralen Steuereinheit jederzeit die Konfiguration ändern.</p>',
        'price' => [
            'perMonth' => '100',
            'fixed' => '15.000',
            'multi' => [
                '1 - 10' => '12.000',
                '10 - 50' => '11.000',
                'ab 50' => '10.000'
            ]
        ],
        'includes' => [
            'inkl. Bodensensoren',
            'Gesamtfläche bis 1ha',
            'inkl. Bewässerungsanlage'
        ],
        'author' => [
            'name' => 'Jan Storm',
            'email' => 'jstorm@smart-farm.de',
            'icon' => 'Team/jstorm.png',
        ]
    ],
    [
        'id' => '2',
        'title' => 'SmartFeeding',
        'icon' => 'Products/feeding.svg',
        'image' => $GLOBALS["imagePath"] . 'Shop/smart-farming-700x460-1.jpg',
        'teaser' => 'Kein lästiges Schleppen und Rennen mehr - sorgen sie für eine optimale Futterversorgung und das Wohl ihrer Tiere!',
        'description' => '<h2>ANGEPASSTER BEDARF</h2><p>Die SmartFeeding Maschinen können entweder im Automatik-Modus betrieben werden (automatische Bedarfsrechnung an Futter, festgelegte Fütterungszeiten & automatische Förderung von Futtermittel). Alle oben gennanten können selbstverständlich jederzeit auf manuell umgestellt werden bzw. Zeiten und Umfang individuell angepasst werden.
<h2>SPARSAM UND PÜNKTLICH</h2><p>Die automatische Bedarfsrechnung sorgt dafür, dass Sie nie wieder zu viel Futtermittel verschwenden. Anhand festgelegter Kennwerte errechnet das System die optimale Futtermenge und schenkt diese beidseitig an ihre Tiere aus ! Am Ende des Tages muss der Futterbehälter lediglich händisch - auf eine festgelegte Größe aufgefüllt werden. Kein lästiges Schleppen und Rennen mehr - sorgen sie für eine optimale Futterversorgung und das Wohl ihrer Tiere !',
        'price' => [
            'perMonth' => '200',
            'fixed' => '25.000',
            'multi' => [
                '1 - 10' => '23.000',
                '10 - 50' => '22.000',
                'ab 50' => '20.000'
            ]
        ],
        'includes' => [
            'inkl. Maschinen & Einbau',
            'Gesamtfläche bis 1ha',
            'inkl. automatische Bedarfsrechnung'
        ],
        'author' => [
            'name' => 'Max Croon',
            'email' => 'mcroon@smart-farm.de',
            'icon' => 'Team/mcroon.png',
        ]
    ],
    [
        'id' => '3',
        'title' => 'SmartGate',
        'icon' => 'Products/gate.svg',
        'image' => $GLOBALS["imagePath"] . 'Shop/Screenshot_1.png',
        'teaser' => 'Machen sie ihren Betrieb mit den SmartGate Systemen deutlich sicherer!',
        'description' => '<h2>INDIVIDUELLE SICHERHEIT</h2><p>Unsere SmartGate Tore können kundenindividuell angepasst und ausgebaut werden (Sonderpreis auf Anfrage). Die Gates wurden nach DIN EN ISO 9001 geprüft und unterlaufen täglich einer strengen Qualitätskontrolle. Machen sie ihren Betrieb mit den SmartGate Systemen deutlich sicherer !</p>
<h2>MAXIMALER SCHUTZ BEI MINIMALEN PLATZ</h2><p>Unsere vollautomatischen steuerbaren Schiebetore wirken durch unsere Reduzierung des durchschnittlichen Flächengewichts sehr leicht und nicht zu massiv, sodass auch die Montagezeit in der Regel um 15% reduziert wird. Desweiteren gibt es auch, wenn gewünscht, die hauseigenen Hubtore - diese öffnen sich vertikal zur Decke und können auch bei engeren Raumverhältnissen für einen optimalen Brand- und Auslaufschutz sorgen.</p>
',
        'price' => [
            'perMonth' => '100',
            'fixed' => '10.000',
            'multi' => [
                '1 - 10' => '9.000',
                '10 - 50' => '8.500',
                'ab 50' => '8.000'
            ]
        ],
        'includes' => [
            'vollautomatischen steuerbaren Schiebetore',
            'Gesamtfläche bis 1ha',
        ],
        'author' => [
            'name' => 'Arne Wilhelm',
            'email' => 'awilhelm@smart-farm.de',
            'icon' => 'Team/awilhelm.png',
        ]
    ],
    [
        'id' => '4',
        'title' => 'SmartLightning',
        'icon' => 'Products/light.svg',
        'image' => $GLOBALS["imagePath"] . 'Shop/Screenshot_1.png',
        'teaser' => 'machen Sie sich keine Sorgen mehr um die Lichverhältnisse!',
        'description' => '<h2>HELL UND EFFIZIENT</h2><p>Das SmartLightning System aus dem Hause SmartFarm wurde speziell auf Effizienz bei gleichbleibender Leistung entwickelt. Die dimmbaren LEDs passen sich automatisch den wechselnden Lichtverhältnissen an oder können bei Bedarf auch manuell ausgerichtet werden. Vermeiden Sie unnötig hohe Stromkosten und "zerbrochene" Glühbirnen.</p>
<h2>HOCHWERTIG</h2><p>Die stabilen Trafos innerhalb der Leuchmittel starten ohne Verzögerung und ohne Flackern. Mit einer Laufzeit von circa 30.000 Arbeitsstunden müssen Sie sich keine Sorgen mehr um die Lichverhältnisse machen ! Die besonders hohe Farbwiedergabe (CRI>80) sorgt für lebendige Farben und einen warmen Lichteffekt. Die angepassten LED´s haben eine extrem geringe Wärmeentwicklung, um mögliche Risiken auszuschließen !</p>
<h2>SPARSAM</h2><p>Selbst bei einem Energieverbrauch von 10kWh/1000h (Verbrauch bei Dauerbetrieb bei gleichbleibender Helligkeit) wird unser SmartLightning System optimal betrieben und versorgt. Auch mehrere Lampen wirken sich nicht negativ auf den Stromverbrauch aus.</p>',
        'price' => [
            'perMonth' => '100',
            'fixed' => '15.000',
            'multi' => [
                '1 - 10' => '14.000',
                '10 - 50' => '13.500',
                'ab 50' => '13.000'
            ]
        ],
        'includes' => [
            'Automatisierte Beleuchtungsanlage',
            'Optimale Nutzung von natürlichem und künstlichem Licht'
        ],
        'author' => [
            'name' => 'Martin',
            'email' => 'mkrawtzow@smart-farm.de',
            'icon' => 'Team/mkrawtzow.png',
        ]
    ],
    [
        'id' => '5',
        'title' => 'SmartSecure',
        'icon' => 'Products/secure.svg',
        'image' => $GLOBALS["imagePath"] . 'Shop/Screenshot_1.png',
        'teaser' => '',
        'description' => '',
        'price' => [
            'perMonth' => '100',
            'fixed' => '15.000',
            'multi' => [
                '1 - 10' => '14.000',
                '10 - 50' => '13.500',
                'ab 50' => '13.000'
            ]
        ],
        'includes' => [
            'inkl. Sicherheitssoftware',
            'Gesamtfläche bis 1ha',
            'Gutes Gefühl inklusive'
        ],
        'author' => [
            'name' => 'Friedemann Kupfer',
            'email' => 'fkupfer@smart-farm.de',
            'icon' => 'Team/fkupfer.png',
        ]
    ],
];
$GLOBALS["partialRootPath"] = ROOT . 'Resources\\Private\\Partials\\';
$GLOBALS["contentFile"] = 'Content.php';
$GLOBALS["errorPage"] = 4;
$GLOBALS["startPage"] = 1;
$GLOBALS["productPage"] = 3;
$GLOBALS["pageMapping"] = [
    1 => [
        "title" => "Startseite",
        "altTitle" => "Home",
        "path" => "Page/Startseite/",
        "url" => "Startseite",
        "config" => [
            "inNav" => true,
            "showHeader" => true,
            "searchable" => true,
        ]
    ],
    2 => [
        "title" => "Shop",
        "path" => "Page/Shop/",
        "url" => "Shop",
        "config" => [
            "inNav" => true,
            "showHeader" => false,
            "searchable" => true,
        ]
    ],
    3 => [
        "title" => "Product",
        "path" => "Page/Product/",
        "url" => "Product",
        "param" => [
            'global' => 'apps',
            'check' => 'title',
            'save' => 'currentProduct'
        ],
        "config" => [
            "inNav" => false,
            "showHeader" => false,
            "searchable" => false,
        ]
    ],
    4 => [
        "title" => "404",
        "path" => "Page/404/",
        "url" => "404",
        "config" => [
            "inNav" => false,
            "showHeader" => false,
            "searchable" => false,
        ]
    ],
    5 => [
        "title" => "Impressum",
        "path" => "Page/Impressum/",
        "url" => "Impressum",
        "config" => [
            "inNav" => false,
            "showHeader" => false,
            "searchable" => true,
        ]
    ],
    6 => [
        "title" => "Kontakt",
        "path" => "Page/Contact/",
        "url" => "Kontakt",
        "config" => [
            "inNav" => true,
            "showHeader" => false,
            "searchable" => true,
        ]
    ],
    7 => [
        "title" => "Basispaket",
        "path" => "Page/Product/Basispaket/",
        "url" => "Basispaket",
        "altTitle" => "Basispaket",
        "config" => [
            "inNav" => false,
            "showHeader" => false,
            "searchable" => true,
        ]
    ],
    8 => [
        "title" => "Warenkorb",
        "path" => "Page/Shop/Cart/",
        "url" => "Shop/Warenkorb",
        "config" => [
            "inNav" => false,
            "showHeader" => false,
            "searchable" => false,
        ]
    ],
    9 => [
        "title" => "Kasse",
        "path" => "Page/Shop/Cashier/",
        "url" => "Shop/Kasse",
        "config" => [
            "inNav" => false,
            "showHeader" => false,
            "searchable" => false,
        ]
    ],
    10 => [
        "title" => "Kasse",
        "path" => "Page/Shop/Cashier/Thanks/",
        "url" => "Shop/Kasse/Danke",
        "config" => [
            "inNav" => false,
            "showHeader" => false,
            "searchable" => false,
        ]
    ],
    11 => [
        "title" => "Suche",
        "path" => "Page/Search/",
        "url" => "Suche",
        "config" => [
            "inNav" => false,
            "showHeader" => false,
            "searchable" => false,
        ]
    ],
    12 => [
        "title" => "Datenschutz",
        "path" => "Page/Datenschutz/",
        "url" => "Datenschutz",
        "config" => [
            "inNav" => false,
            "showHeader" => false,
            "searchable" => false,
        ]
    ]
];
$GLOBALS['partners'] = [
    [
        'src' => 'Resources/Public/Images/Kooperationen/logo_zalf.png',
        'alt' => '​​​​Leibniz-Zentrum für Agrarlandschaftsforschung (ZALF) e. V.',
        'link' => 'http://www.zalf.de',
    ],
    [
        'src' => 'Resources/Public/Images/Kooperationen/smartakis-262x80-rounded-bg.png',
        'alt' => 'Anbieter von Smart Farming Lösungen',
        'link' => 'https://www.smart-akis.com',
    ],
    [
        'src' => 'Resources/Public/Images/Kooperationen/DLG.jpg',
        'alt' => 'DLG e.V. - (Deutsche Landwirtschafts-Gesellschaft)',
        'link' => 'https://www.dlg.org',
    ],
    [
        'src' => 'Resources/Public/Images/Kooperationen/P13_DTA_EurAgeng_New.png',
        'alt' => 'European Society for Agricultural Engineers',
        'link' => 'http://www.eurageng.eu/',
    ],
    [
        'src' => 'Resources/Public/Images/Kooperationen/P2_WUR.png',
        'alt' => 'To explore the potential of nature to improve the quality of life',
        'link' => 'https://www.wur.nl/',
    ],
    [
        'src' => 'Resources/Public/Images/Kooperationen/smartfarm-logo.png',
        'alt' => 'Smartfarms intelligente Sensoren sind einfach im Gebrauch und liefern Echtzeit-Informationen.',
        'link' => 'https://www.smartfarm.nl/de/',
    ],
    [
        'src' => 'Resources/Public/Images/Kooperationen/P8_Delphy_logo_cmyk_1945U_5.png',
        'alt' => 'Delphy',
        'link' => 'http://delphy.nl/en/',
    ],
    [
        'src' => 'Resources/Public/Images/Kooperationen/acta_logo.png',
        'alt' => 'Les instituts techniques agricoles du réseau Acta',
        'link' => 'http://www.acta.asso.fr/',
    ],
    [
        'src' => 'Resources/Public/Images/Kooperationen/biosense_logo.png',
        'alt' => 'Biosense',
        'link' => 'https://biosense.rs/?page_id=6597&lang=en',
    ],
];

if (!isset($_GET['path'])) {
    $_GET['path'] = "";
}

$speakingUrl = new SpeakingUrl($_GET['path']);
$speakingUrl->executeMapping();

$useragent= isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'chrome';

// see https://stackoverflow.com/questions/4117555/simplest-way-to-detect-a-mobile-device
$GLOBALS['isMobile'] = preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));