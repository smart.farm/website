<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 19.03.2019
 * The Header Content
 */
?>

<div id="header">
    <div class="slick-slider">
       <div>
            <img class="image rotate-earth" src="<?php echo $GLOBALS["webRoot"]?>Resources/Public/Images/hero-1.jpg"/>
            <div class="content-wrapper">
                <div class="text-wrapper">
                    <h1 class="headline">Die Smart<strong>Farm</strong> GmbH</h1>
                    <h3>Wie SmartHome, nur für <strong>deine</strong> Farm!</h3>
                </div>
                <div class="action-buttons">
                    <a class="btn btn-primary" href="<?php echo $GLOBALS["webRoot"]?>startseite#uerberuns"><span class="arrow">Über uns</span></a>
                    <a class="btn btn-primary" href="<?php echo $GLOBALS["webRoot"]?>shop"><span class="arrow">Zum Shop</span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="downarrow-wrapper">
        <img src="<?php echo $GLOBALS["webRoot"]?>Resources/Public/Icons/down-arrow.svg"/>
    </div>
</div>