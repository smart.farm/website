<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 19.03.2019
 * The Navigation Content
 */

function generateNav() {
    foreach ($GLOBALS["pageMapping"] as $page) {
        if ($page['config']['inNav']) {
            echo '<li class="nav-item '.($GLOBALS['currentPage'] == $page ? 'active' : '').'">';
            echo '    <a class="nav-link" href="' . $GLOBALS["webRoot"] . strtolower($page['url']) . '">'.(isset($page['altTitle']) ? $page['altTitle'] : $page['title']);
            if ($GLOBALS['currentPage'] == $page) {
                echo ' <span class="sr-only">(current)</span>';
            }
            echo '</a></li>';
        }
    }
}

?>

<nav class="navbar navbar-expand-lg navbar-dark <?php echo $GLOBALS['currentPage']['config']['showHeader'] ? 'fixed-top' : 'not-fixed'?> <?php echo $GLOBALS['currentPage']['config']['showHeader'] ? 'trans' : 'bg-dark';?>">
    <a class="navbar-brand" href="<?php echo $GLOBALS['webRoot']?>">
        <img class="logo-img" src="<?php echo $GLOBALS['webRoot']?>Resources/Public/Images/logo_icon.svg"/>
        <img class="logo-img" src="<?php echo $GLOBALS['webRoot']?>Resources/Public/Images/logo_text.svg"/>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto cart">
            <?php
                generateNav();
            ?>
            <li class="nav-item dark btn">
                <a class="nav-link" href="<?php echo $GLOBALS['webRoot']?>shop/warenkorb">Warenkorb
                    <span class="badge badge-danger shopping-badge">6</span></a>
            </li>
        </ul>
        <form id="search-form" class="form-inline my-2 my-lg-0" method="post" action="<?php echo $GLOBALS['webRoot']; ?>Suche">
            <input class="form-control mr-sm-2" type="search" placeholder="Suchen" aria-label="Search" name="q">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Suche</button>
        </form>
        <div class="cart">
            <a class="btn ml-0 border-0 light" href="<?php echo $GLOBALS['webRoot']?>shop/warenkorb">
                <img width="32" height="32" class="noreplace" src="<?php echo $GLOBALS['publicPath']?>Icons/shopping-cart.svg"/>
                <span class="badge badge-danger shopping-badge" style="display: none;">0</span>
            </a>
        </div>
    </div>
</nav>