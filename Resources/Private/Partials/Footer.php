<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 19.03.2019
 * The Footer Content
 */
?>

<div class="cta-contact-wrapper">
    <div class="section">
        <div class="container">
            <div class="cta-contact">
                <div class="row aos-wrapper">
                    <div class="col-sm-6" data-aos="fade-right">
                        <h3 class="right">Sie wünschen sich persönlichen Kontakt?</h3>
                    </div>
                    <div class="col-sm-6" data-aos="fade-left">
                        <h3 class="display-4">04892/278465</h3>
                        <p>Rufen Sie an!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="partner-wrapper parallax">
    <div class="container-fluid">
        <h2 class="special-header-wrapper noruler"><p class="special-header">Kooperationen</p></h2>
        <div class="partner-slider-wrapper container-fluid">
            <div class="partner partner-slider">
                <?php
                foreach ($GLOBALS['partners'] as $file) {
                    if ($file !== "." && $file != "..") {
                        echo '<li class="slide"><a href="' . $file['link'] . '"><img src="' . $GLOBALS['webRoot'] . $file['src'] . '" alt="' . $file['alt'] . '"/></a></li>';
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>

<div class="footer-wrapper">
    <div class="container">
        <div id="footer">
            <div class="row">
                <div class="col-md-3">
                    <ul>
                        <li><a href="<?php echo $GLOBALS['webRoot']; ?>startseite">Startseite</a></li>
                        <li><a href="<?php echo $GLOBALS['webRoot']; ?>shop">Shop</a></li>
                        <li><a href="<?php echo $GLOBALS['webRoot']; ?>kontakt">Kontakt</a></li>
                        <li><a href="<?php echo $GLOBALS['webRoot']; ?>impressum">Impressum</a></li>
                        <li><a href="<?php echo $GLOBALS['webRoot']; ?>datenschutz">Datenschutz</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <img class="logo" src="<?php echo $GLOBALS['webRoot']; ?>Resources/Public/Images/logo_text.svg"/>
                    <p>Holzkoppel 1<br>
                        22869 Schenefeld</p>
                    <p>Tel.: <a href="tel://04078465">040/278465</a><br>
                        Mail.: <a href="mailto://info@smart-farm.de">info@smart-farm.de</a></p>
                </div>
                <div class="col-md-6">
                    <h3>Unser Newsletter</h3>
                    <p>Melden Sie sich zu unserem Newsletter an, um über unsere neuesten Produkte laufend informiert zu werden!</p>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-newsletter">Ihre Email</span>
                        </div>
                        <input type="text" class="form-control" aria-label="email" aria-describedby="inputGroup-sizing-newsletter">
                        <div class="input-group-append">
                            <button class="btn btn-outline-success" type="button" id="button-newsletter">Anmelden</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>