<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 02.05.2019
 * The Cart Page. Displays the current items in the cart via js
 */
?>

<div class="section">
    <div class="container shopping-cart">
        <h1 class="display-1">Waren&shy;korb</h1>
        <div class="calculation">
            <h2>Ihr Einkauf:</h2>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Hektar</th>
                    <th scope="col">Produkt</th>
                    <th scope="col">Preis</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="items">
                </tbody>
                <tfoot>
                    <tr>
                        <th width="5%" scope="row">Gesamt</th>
                        <td width="20%"></td>
                        <td width="30%"></td>
                        <td width="30%" id="priceSum">Ne Mille</td>
                        <td width="20%"></td>
                    </tr>
                </tfoot>
            </table>
            <a class="btn btn-success btn-lg mb-5" href="<?php echo $GLOBALS['webRoot']; ?>shop/kasse">
                <img class="mr-2" width="32" height="32" src="/smart-farm/Resources/Public/Icons/shopping-cart.svg">Zur Kasse</a>
        </div>
    </div>
</div>

<script>
    var APPS = <?php echo json_encode($GLOBALS['apps']) ?>;

    var template =
        '  <tr>\n' +
        '        <th scope="row">{{index}}</th>\n' +
        '        <td>{{count}} <small>ha</small></td>\n' +
        '        <td><img src="{{img}}"/><a href="<?php echo $GLOBALS['webRoot']; ?>{{link}}">{{title}}</a></td>\n' +
        '        <td>' +
        '           <p>{{price}} €</p>' +
        '           <p>{{priceMonthly}} € / mo</p>' +
        '        </td>\n' +
        '        <td>' +
        '           <button type="button" class="btn btn-outline-danger" sc-remove="{{id}}">Entfernen</button>' +
        '        </td>\n' +
        '    </tr>';
    window.spCalc = {
        fixed: 0,
        month: 0
    };

    (function () {
        $(document).ready(function () {
            ShoppingCart.items = ShoppingCart.getItems();
            for (var index in ShoppingCart.items) {
                if (ShoppingCart.items.hasOwnProperty(index)) {

                    var item = ShoppingCart.items[index];
                    var app = {};

                    if(item.id === ShoppingCart.idMain) {
                        item.count = '∞';
                        app = {
                            title: 'Basispaket',
                            icon: 'Products/base.png',
                            price: {
                                perMonth: "500",
                                fixed: "0"
                            }
                        }
                    } else {
                        app = APPS.filter(function (obj) {
                            return "" + obj.id === "" + item.id
                        })[0];
                    }

                    var count = parseInt(item.count);
                    if(!isInteger(count)) { count = 0; }
                    var price = (count * parseInt(app.price.fixed.replace('.','')));

                    window.spCalc.fixed += price;
                    window.spCalc.month += parseInt(app.price.perMonth.replace('.',''));

                    $('#items').append(template
                            .replace('{{img}}', '<?php echo $GLOBALS['iconPath'];?>' + app.icon)
                            .replace('{{title}}', app.title)
                            .replace('{{link}}', (item.id !== ShoppingCart.idMain ? ('product/' + (app.title)) : 'basispaket'))
                            .replace('{{count}}', item.count)
                            .replace('{{index}}', parseInt(index) + 1)
                            .replace('{{price}}', SmartFarm.util.numberWithCommas(price))
                            .replace('{{priceMonthly}}',SmartFarm.util.numberWithCommas(app.price.perMonth))
                            .replace('{{id}}',item.id)
                    );
                }
            }
            if(ShoppingCart.items.length < 1) {
                $('.shopping-cart').append('<h2>Ihr Warenkorb ist derzeit leer.</h2><h4 class="mb-5">Sie können in unserem Shop Produkte erwerben, die dann anschließend hier gelistet werden</h4>');
                $('.calculation').hide();
            } else {
                $('.calculation').show();
            }

            ShoppingCart.registerRemoveFromCartButtons();
            $('#priceSum').html(
                ' <p>'+SmartFarm.util.numberWithCommas(window.spCalc.fixed)+' €</p>' +
                ' <p>'+SmartFarm.util.numberWithCommas(window.spCalc.month)+' € / mo</p>');

            function isInteger(num) {
                return (num ^ 0) === num;
            }
        })
    }());
</script>