<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 17.04.2019
 * The Shop Page
 */

function drawTemplate($app, $preview)
{
    echo '<div class="app-wrapper ';
    echo $preview ? 'preview' : 'col-md-6';
    echo '"';
    if ($preview) echo 'style="background-image: url('.$app['image'].')"';
    else echo 'data-aos="fade-up"';
    echo '>';
    $template = file_get_contents(getcwd() . '\Page\Shop\appTemplate.html');
    $template = str_replace("{{id}}", $app['id'], $template);
    $template = str_replace("{{link}}", $GLOBALS["webRoot"] . 'product/' . strtolower($app['title']), $template);
    $template = str_replace("{{icon}}", $GLOBALS["iconPath"] . $app['icon'], $template);
    $template = str_replace("{{image}}", $app['image'], $template);
    $template = str_replace("{{title}}", $app['title'], $template);
    $template = str_replace("{{teaser}}", $app['teaser'], $template);
    $template = str_replace("{{author.icon}}", $GLOBALS["iconPath"] . $app['author']['icon'], $template);
    $template = str_replace("{{author.name}}", $app['author']['name'], $template);
    $template = str_replace("{{author.email}}", $app['author']['email'], $template);
    $template = str_replace("{{arrow}}", $preview ? '<span class="arrow">Jetzt entdecken!</span>' : 'Jetzt entdecken!', $template);
    echo $template . '</div>';
}

?>

<div class="shop section">
    <div class="app-main" style="background-image: url('<?php echo $GLOBALS['imagePath']?>Shop/basis.jpeg')">
        <div class="content-wrapper">
            <h2 class="noruler">Unser Basispaket</h2>
            <h3 class="text-center">Das Grundangebot für jedermann!</h3>
            <div class="buttons text-center">
                <a class="btn border-primary" href="<?php echo $GLOBALS['webRoot']; ?>basispaket">
                    <span class="arrow">Jetzt entdecken!</span>
                </a>
            </div>
        </div>
    </div>
    <div class="app-secondary">
        <div class="row">
            <div class="col-md-6">
                    <?php
                    drawTemplate($GLOBALS['apps'][0], true);
                    ?>
            </div>
            <div class="col-md-6">
                    <?php
                    drawTemplate($GLOBALS['apps'][1], true);
                    ?>
            </div>
        </div>
    </div>
    <div class="container">
        <h3 class="text-center">Hier finden Sie unsere gesamte Produktpalette</h3>
        <div class="apps">
            <div class="row">
                <?php
                for ($i = 2; $i < sizeof($GLOBALS['apps']); $i++) {
                    $app = $GLOBALS['apps'][$i];
                    drawTemplate($app, false);
                }
                ?>
            </div>
        </div>
    </div>
</div>
