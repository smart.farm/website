<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 03.05.2019
 * The Cashier Page
 */
?>

<div class="section mb-5">
    <div class="container">
        <h1 class="display-1">Kasse</h1>
        <h2>Schritt 1: Rechnungsadresse</h2>
        <div class="row">
            <div class="col-md-4 order-md-2 mb-4">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-muted">Sie sind bereits Kunde?</span>
                </h4>
                <div class="card">
                    <h5 class="card-header">Melden Sie sich an.</h5>
                    <div class="card-body">
                        <form class="needs-validation login" novalidate call="<?php echo $GLOBALS['webRoot']; ?>Resources/Public/Service/FormSubmit.php">
                            <div class="mb-3">
                                <label for="user">Email</label>
                                <input type="email" class="form-control" name="user" placeholder="you@example.com" required>
                                <div class="invalid-feedback">
                                    Bitte eine gültige Email Adresse angeben.
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="pass">Passwort</label>
                                <input type="password" class="form-control" name="pass" placeholder="" value="" required>
                                <div class="invalid-feedback">
                                    Ein Passwort ist erforderlich.
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Login</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8 order-md-1">
                <form class="needs-validation" novalidate action="<?php echo $GLOBALS['webRoot']; ?>Shop/Kasse/Danke" method="post">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="firstName">Vorname</label>
                            <input type="text" class="form-control" id="firstName" placeholder="" value="" required>
                            <div class="invalid-feedback">
                                Valid first name is required.
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="lastName">Nachname</label>
                            <input type="text" class="form-control" id="lastName" placeholder="" value="" required>
                            <div class="invalid-feedback">
                                Valid last name is required.
                            </div>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="email">Email <span class="text-muted">(Optional)</span></label>
                        <input type="email" class="form-control" id="email" placeholder="you@example.com">
                        <div class="invalid-feedback">
                            Please enter a valid email address for shipping updates.
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="address">Adresse</label>
                        <input type="text" class="form-control" id="address" placeholder="" required>
                        <div class="invalid-feedback">
                            Please enter your shipping address.
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="address2">Adresszusatz <span class="text-muted">(Optional)</span></label>
                        <input type="text" class="form-control" id="address2" placeholder="">
                    </div>

                    <div class="row">
                        <div class="col-md-5 mb-3">
                            <label for="country">Bundesland</label>
                            <select class="custom-select d-block w-100" id="country" required>
                                <option value="">Auswählen...</option>
                                <option>Baden-WürttembergBayern</option>
                                <option>Berlin</option>
                                <option>Brandenburg</option>
                                <option>Bremen</option>
                                <option>Hamburg</option>
                                <option>Hessen</option>
                                <option>Mecklenburg-Vorpommern</option>
                                <option>Niedersachsen</option>
                                <option>Nordrhein-Westfalen</option>
                                <option>Rheinland-Pfalz</option>
                                <option>Saarland</option>
                                <option>Sachsen</option>
                                <option>Sachsen-Anhalt</option>
                                <option>Schleswig-Holstein</option>
                                <option>Thüringen</option>
                            </select>
                            <small id="countryHelp" class="form-text text-muted">Lieferungen bisher nur nach
                                Deutschland
                            </small>
                            <div class="invalid-feedback">
                                Please select a valid country.
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="zip">Postleitzahl</label>
                            <input type="text" class="form-control" id="zip" placeholder="" required>
                            <div class="invalid-feedback">
                                Zip code required.
                            </div>
                        </div>
                    </div>
                    <hr class="mb-4">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="same-address">
                        <label class="custom-control-label" for="same-address">Zustelladresse ist entspricht der
                            Rechnungsadresse</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="save-info">
                        <label class="custom-control-label" for="save-info">Lege ein Konto mit diesen Daten
                            an.</label>
                        <small class="form-text text-muted">Sie bekommen dann eine Bestätigungsmail und können den
                            Login bei der nächsten Bestellung verwenden.
                        </small>
                    </div>
                    <hr class="mb-4">

                    <h2>Schritt 2: Bezahlinformationen</h2>

                    <div class="d-block my-3">
                        <div class="custom-control custom-radio">
                            <input id="credit" name="paymentMethod" type="radio" class="custom-control-input"
                                   checked
                                   required>
                            <label class="custom-control-label" for="credit">Kreditkarte</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input id="debit" name="paymentMethod" type="radio" class="custom-control-input"
                                   required>
                            <label class="custom-control-label" for="debit">EC-Karte</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input id="paypal" name="paymentMethod" type="radio" class="custom-control-input"
                                   required>
                            <label class="custom-control-label" for="paypal">PayPal</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="cc-name">Name</label>
                            <input type="text" class="form-control" id="cc-name" placeholder="" required>
                            <small class="text-muted">Der vollständige Name, wie auf der Karte zu finden</small>
                            <div class="invalid-feedback">
                                Name on card is required
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="cc-number">Kartennummer</label>
                            <input type="text" class="form-control" id="cc-number" placeholder="" required>
                            <div class="invalid-feedback">
                                Credit card number is required
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <label for="cc-expiration">Gültig bis</label>
                            <input type="text" class="form-control" id="cc-expiration" placeholder="" required>
                            <div class="invalid-feedback">
                                Expiration date required
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="cc-cvv">Kontrollnummer</label>
                            <input type="text" class="form-control" id="cc-cvv" placeholder="" required>
                            <div class="invalid-feedback">
                                Security code required
                            </div>
                        </div>
                    </div>
                    <hr class="mb-4">
                    <button type="submit" class="btn btn-primary btn-lg btn-block"
                            href="<?php echo $GLOBALS['webRoot'] ?>shop/kasse/danke">Continue to checkout
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-img-title text-center m-4">
                    <img width="128" height="64" src="<?php echo $GLOBALS['publicPath']?>Images/logo.svg"/>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3>Hallo <span class="data name"></span>,</h3>
                <h5>Sie wurden erfolgreich eingeloggt.</h5>
                <p>Ihre Bestellung wird mit dem klick auf 'Jetzt bestellen' abgeschlossen.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <a class="btn btn-primary" href="<?php echo $GLOBALS['webRoot']; ?>Shop/Kasse/Danke">Jetzt bestellen</a>
            </div>
        </div>
    </div>
</div>
<div class="cookie-wrapper">
    <div class="container">
        <div id="cookie-note">
            <p>Auf dieser Website werden sogenannte Cookies verwendet. Es ist für uns selbstverständlich, dass dieses immer im gesetzlich vorgegebenen Rahmen geschieht. Nähere Informationen können Sie unserer Datenschutzinformation entnehmen.</p>
            <div class="buttons">
                <p>Sind Sie mit der Nutzung von Cookies einverstanden?</p>
                <a class="btn btn-success" id="enable-cookie">Ja</a>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo $GLOBALS['webRoot']; ?>Resources/Public/Javascripts/FormValidator.js"></script>