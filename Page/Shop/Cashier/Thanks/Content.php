<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 05.05.2019
 */
?>
<div class="section mb-5">
    <div class="container">
        <h1 class="display-2">Vielen Dank für ihren Einkauf.</h1>
        <h2 class="noruler">Ihre Bestellung wurde aufgenommen und Sie werden über sämtliche Updates per Mail benachrichtigt.</h2>
    </div>
</div>

<script>
    ShoppingCart.removeItems();
</script>
