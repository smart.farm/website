<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 17.04.2019
 * The Contact Page
 */
?>

<div class="section">
    <div class="container">
        <h1 class="display-3">Kontakt</h1>
        <div class="row">
            <div class="col-md-9">
                <form>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Email Adresse</label>
                        <input type="email" class="form-control" id="exampleFormControlInput1"
                               placeholder="name@example.com">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput2">Betreff</label>
                        <input type="text" class="form-control" id="exampleFormControlInput2" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Text</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="4"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Absenden</button>
                </form>
            </div>
            <div class="col-md-3">
                <div class="card mt-3 mb-3">
                    <img class="card-img-top fix-height" src="<?php echo $GLOBALS['publicPath']; ?>Images/team.jpg"
                         alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Kontaktieren Sie uns!</h5>
                        <p class="card-text">Rufen Sie an, oder schicken Sie uns eine Email! alternativ finden Sie links
                            ein Kontaktformular</p>
                        <a href="tel://040278465" class="btn btn-primary">Telefon</a>
                        <a href="mailto:janstormi@gmail.com" class="btn btn-primary">Email</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section">
    <h2 class="display-4 text-center noruler">Unser Standort</h2>
    <p class="text-center">Kommen Sie uns doch besuchen, wir sind rund um die Uhr für sie da!</p>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d233871.29742696442!2d9.87020472928623!3d53.5715012416795!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b183fef891d6fb%3A0x5cb5c4d479a03a1!2sHolzkoppel+1%2C+22869+Schenefeld!5e0!3m2!1sde!2sde!4v1557813662997!5m2!1sde!2sde"
            frameborder="0" style="border:0;width: 100%;height: 400px;"></iframe>
</div>