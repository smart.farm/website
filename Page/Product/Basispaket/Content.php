<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 30.04.2019
 * The Basis-Package Page
 */
?>

<div class="product detail">
    <div class="header bg-dark">
        <div class="container">
            <p class="breadcrumbs"><a href="<?php echo $GLOBALS['webRoot']; ?>startseite">Home</a> / <a href="<?php echo $GLOBALS['webRoot']; ?>shop">Products</a>
                /</p>
            <div class="app">
                <img src="<?php echo $GLOBALS['webRoot']; ?>Resources/Public/Icons/Products/base.png"/>
                <h1 class="display-1">Basis&shy;paket</h1>
            </div>
        </div>
    </div>
    <div class="gradient">
        <div class="gradient-content">
            <div class="container">
                <p class="teaser">Alles aus einer Hand!</p>
            </div>
        </div>
    </div>
</div>
<div class="section detail pt-4">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mb-5">
                <h2 class="display-2" style="font-size: 4rem;">Das ist unser Basispaket</h2>
                <p>Das SmartFarm Basispaket ist die erste Anlaufstation und bietet unseren Kunden die optimale
                    Grundlage zur technischen Revolutionierung ihres Agrarbetriebes.</p>
                <p>Der Vorteil: Sie können beliebig
                    viele Module an die Basissteuerung anbinden – ganz ohne
                    Mehrkosten! Behalten Sie ihren Betrieb einfach und effizient im Überblick!</p>
                <p>Bis dato können zusätzliche Module problemlos und ohne Bearbeitungskosten an das Basispaket
                    angebunden werden: SmartFeed, SmartLightning, SmartGate, SmartSecure &amp; SmartWater</p>
                <p>Die einfach zu bedienende Benutzeroberfläche ermöglich es ihnen, die Gerätschaften stets unter
                    Kontrolle zu haben – ganz ohne technische Vorkenntnisse. Steigern sie jetzt spielerisch leicht die
                    Effizienz und Kompetenz ihres Betriebes und ihren damit verbundenen Umsatz!</p>
            </div>
            <div class="col-md-4">
                <div class="container">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="my-0 font-weight-normal">Basis</h4>
                        </div>
                        <div class="card-body">
                            <h1 class="card-title pricing-card-title">500€
                                <small class="text-muted">/ mo</small>
                            </h1>
                            <ul class="list-unstyled mt-3 mb-4">
                                <li>Dynamisch erweiterbar</li>
                                <li>Zentrale Steuerungssoftware</li>
                                <li>Kontrolle per App & Pc</li>
                                <li>3 Jahre Support</li>
                                <li>Regelmäßige Updates</li>
                                <li>Benötigt min. 1 Produkt</li>
                            </ul>
                            <!-- TODO add to cart -->
                            <a class="btn btn-lg btn-block btn-outline-primary" sc-main
                               href="<?php echo $GLOBALS['webRoot']; ?>shop">Erweitern</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div data-aos="fade-left">
            <div class="row">
                <div class="col-md-3 center-parent">
                    <img class="center-middle" src="<?php echo $GLOBALS['iconPath']; ?>behavior.svg" height="100px" width="auto"/>
                </div>
                <div class="col-md-9">
                    <h2>DYNAMISCH &amp; FLEXIBEL</h2>
                    <p>Sie benötigen ein zusätzliches Modul? Ihre Betriebsgröße hat sich verändert
                        und Sie müssen neue Anforderungen verwalten? Das SmartFarm Basispaket ist ihre zentrale
                        Steuerung für alle Komponenten unserer automatisierten Landwirtschaft. Das Steuerungsmodul ist
                        ab der Bestellung Ihres ersten Moduls erforderlich und verbindlich.</p>
                </div>
            </div>
        </div>
        <div data-aos="fade-left">
            <div class="row">
                <div class="col-md-3 center-parent">
                    <img class="center-middle" src="<?php echo $GLOBALS['iconPath']; ?>control.svg" height="100px" width="auto"/>
                </div>
                <div class="col-md-9">
                    <h2>EASY CONTROL</h2>
                    <p>Die Gerätschaften können sie spielend leicht und ohne technische Vorkenntnisse
                        über unsere Benutzeroberfläche steuern. Durch unsere mobile App sowie Desktopanwendung
                        können sie auch von unterwegs individuelle Einstellungen vornehmen und die Produktion bzw.
                        Bewirtschaftung in Echtzeit anpassen.</p>
                </div>
            </div>
        </div>
        <div data-aos="fade-left">
            <div class="row">
                <div class="col-md-3 center-parent">
                    <img class="center-middle" src="<?php echo $GLOBALS['iconPath']; ?>support.svg" height="100px" width="auto"/>
                </div>
                <div class="col-md-9">
                    <h2>IMMER FÜR SIE DA</h2>
                    <p>Der inkludierte 3 Jahres Support steht ihnen jeder Zeit zur Verfügung. Sie haben
                        technische Fragen oder möchten ein zusätzliches Modul implementieren? Zögern sie nicht uns zu
                        Fragen – unsere Supporter stehen ihnen gerne mit Rat und Tat rund um das Thema automatisierte
                        Landwirtschaft zur Verfügung.</p>
                </div>
            </div>
        </div>
        <div data-aos="fade-left" class="mb-5">
            <div class="row">
                <div class="col-md-3 center-parent">
                    <img class="center-middle" src="<?php echo $GLOBALS['iconPath']; ?>tools.svg" height="100px" width="auto"/>
                </div>
                <div class="col-md-9">
                    <h2>STETIGE VERBESSERUNG</h2>
                    <p>Damit sie auch im laufenden Betrieb nicht an Effizient verlieren, arbeiten
                        wir täglich mit Hochdruck an Verbesserungen für Steuerung und Maschinen – bleiben sie immer
                        wettbewerbs- und konkurrenzfähig!</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="cookie-wrapper">
    <div class="container">
        <div id="cookie-note">
            <p>Auf dieser Website werden sogenannte Cookies verwendet. Es ist für uns selbstverständlich, dass dieses immer im gesetzlich vorgegebenen Rahmen geschieht. Nähere Informationen können Sie unserer Datenschutzinformation entnehmen.</p>
            <div class="buttons">
                <p>Sind Sie mit der Nutzung von Cookies einverstanden?</p>
                <a class="btn btn-success" id="enable-cookie">Ja</a>
            </div>
        </div>
    </div>
</div>