<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 26.03.2019
 * The Product Page. Displays a single App selected by url parameter
 */

$apps = $GLOBALS['apps'];
$app = $GLOBALS['currentProduct'];
?>

<div class="product detail">
    <div class="header bg-dark">
        <div class="container">
            <p class="breadcrumbs"><a href="<?php echo $GLOBALS['webRoot']; ?>startseite">Home</a> / <a href="<?php echo $GLOBALS['webRoot']; ?>shop">Products</a> /</p>
            <div class="app">
                <img src="<?php echo $GLOBALS["iconPath"] . $app['icon']; ?>"/>
                <h1 class="display-1"><?php echo $app['title']; ?></h1>
            </div>
            <div class="author">
                <div class="author-wrapper image-wrapper">
                    <img src="<?php echo $GLOBALS["iconPath"] . $app['author']['icon']; ?>"/>
                </div>
                <div class="author-wrapper">
                    <p class="label">Author</p>
                    <p><?php echo $app['author']['name']; ?></p>
                </div>
                <div class="author-wrapper">
                    <p class="label">Contact</p>
                    <a href="mailto:<?php echo $app['author']['email']; ?>"><?php echo $app['author']['email']; ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="gradient">
        <div class="gradient-content">
            <div class="container">
                <p class="teaser"><?php echo $app['teaser']; ?></p>
            </div>
        </div>
    </div>
</div>

<div class="section detail">
    <div class="container">
        <div class="row">
            <div class="col-9">
                <p><?php echo $app['description']?></p>
            </div>
            <div class="col-3">
                <?php
                    if(isset($app['showcase']))
                    echo '<a href="'.$app['showcase'].'" target="_blank"><img class="mt-3" width="100%" height="auto" src="'. $app['showcase'].'" /></a>';
                ?>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
            <h2 class="display-2 noruler">Kosten</h2>
            <p class="lead">Hier finden Sie eine Übersicht unseres Preismodells. Das Basispaket dient als Grundlage für alle Module. Daher können Sie dies ignorieren, sollten Sie bereits unser Basispaket besitzen.</p>
        </div>
        <div class="card-deck mb-3 text-center">
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Basis</h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title">500€ <small class="text-muted">/ mo</small>
                    </h1>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>Dynamisch erweiterbar</li>
                        <li>Zentrale Steuerungssoftware</li>
                        <li>Kontrolle per App & Pc</li>
                        <li>3 Jahre Support</li>
                        <li>Regelmäßige Updates</li>
                     </ul>
                    <a class="btn btn-lg btn-block btn-outline-primary" href="<?php echo $GLOBALS['webRoot']; ?>basispaket">Details</a>
                </div>
            </div>
            <div class="plus">
                <p class="display-1 symbol">+</p>
            </div>
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Modul <?php echo $GLOBALS['currentProduct']['title'] ?></h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title"><?php echo $GLOBALS['currentProduct']['price']['perMonth'] ?>€
                        <small class="text-muted">/ mo</small>
                    </h1>
                    <h2 class="noruler">
                       <?php echo $GLOBALS['currentProduct']['price']['fixed'] ?>€ <small class="text-muted">Einmalig</small>
                    </h2>
                    <ul class="list-unstyled mt-3 mb-4">
                        <?php
                            foreach ($GLOBALS['currentProduct']['includes'] as $include) {
                                echo "<li>$include</li>";
                            }
                        ?>
                    </ul>
                    <button
                            type="button"
                            class="btn btn-lg btn-block btn-primary"
                            sc-id="<?php echo $GLOBALS['currentProduct']['id']?>"
                            data-toggle="modal"
                            data-target="#thankYou">Jetzt bestellen</button>
                </div>
            </div>
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Ab 1 Hektar</h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title noruler">
                        <?php echo reset($GLOBALS['currentProduct']['price']['multi']) ?>€<small class="text-muted"> / ha</small>
                    </h1>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>Für <?php echo key($GLOBALS['currentProduct']['price']['multi']) ?> ha</li>
                        <li>Keine monatlichen Zusatzkosten</li>
                        <li>Kostengünstigere Anschaffung</li>
                        <li>Keine weiteren Verwaltungskosten</li>
                        <li>Leicht Erweiterbar</li>
                    </ul>
                    <button type="button" class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target="#hectarePrice">Hektarpreise</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section pricecalculator">
    <div class="container">
        <h2 class="display-2 noruler" id="pricecalculator">Preisrechner</h2>
        <h3>Geben Sie hier an, für wie viel Hektar Sie bestellen wollen:</h3>
        <div id="input-wrapper">
            <input type="range" id="rangeslider" min="0" max="100" value="20" step=".5"
                   oninput="updateOutput(value, true)"
                   onchange="deactivate()"
                   onmouseup="deactivate()"
            >
            <div id="reel">
                <div id="rn"></div>
            </div>
            <div id="static-output"></div>
        </div>
        <h3>Preis pro Hektar: <span id="price-hecatre">0</span> €</h3>
        <h3 class="mb-3">Endpreis: <span id="calc-price">0</span> € - <?php echo $GLOBALS['currentProduct']['price']['perMonth'] ?>€<small> / mo</small></h3>
        <button
                type="button"
                class="mb-5 btn buy btn-lg btn-primary"
                sc-id="<?php echo $GLOBALS['currentProduct']['id']?>"
                data-toggle="modal"
                data-target="#thankYou">Jetzt bestellen</button>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="hectarePrice" tabindex="-1" role="dialog" aria-labelledby="hectarePriceTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Hektarpreise für <?php echo $GLOBALS['currentProduct']['title']; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php
                    foreach ($GLOBALS['currentProduct']['price']['multi'] as $key => $value) {
                        echo "<div class=\"media mb-3\">
                              <div class=\"media-body\">
                                <h5 class=\"mt-0\">$key Hektar</h5>
                                $value €
                              </div>
                            </div>";
                    }
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="thankYou" tabindex="-1" role="dialog" aria-labelledby="thankYouTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Vielen Dank!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3>Das Produkt wurde zu ihrem Warenkorb hinzugefügt</h3>
                <div class="text-center m-4">
                    <img width="64" height="64" src="<?php echo $GLOBALS['publicPath']?>Icons/shopping-cart-dark.svg"/>
                </div>
                <p>Sie finden es jederzeit, indem Sie ganz oben rechts auf den Warenkorb klicken.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <a class="btn btn-primary" href="<?php echo $GLOBALS['webRoot']?>shop/warenkorb">Zum Warenkorb</a>
            </div>
        </div>
    </div>
</div>

<script>
    var PRICES = JSON.parse('<?php
        echo json_encode($GLOBALS['currentProduct']['price'])
    ?>');
</script>
<div class="cookie-wrapper">
    <div class="container">
        <div id="cookie-note">
            <p>Auf dieser Website werden sogenannte Cookies verwendet. Es ist für uns selbstverständlich, dass dieses immer im gesetzlich vorgegebenen Rahmen geschieht. Nähere Informationen können Sie unserer Datenschutzinformation entnehmen.</p>
            <div class="buttons">
                <p>Sind Sie mit der Nutzung von Cookies einverstanden?</p>
                <a class="btn btn-success" id="enable-cookie">Ja</a>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo $GLOBALS['webRoot'];?>Resources/Public/Javascripts/Vendor/rangeSlider.js"></script>