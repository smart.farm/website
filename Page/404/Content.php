<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 02.04.2019
 * The default 404 Page
 */
?>

<div id="header" class="medium">
    <div class="slick-slider medium">
        <div>
            <img class="image" src="<?php echo $GLOBALS["webRoot"]?>Resources/Public/Images/gruen.jpg"/>
            <div class="content-wrapper">
                <div class="text-wrapper">
                    <h1 class="display-1">404 nicht gefunden.</h1>
                    <h3>Das tut uns leid.</h3>
                </div>
            </div>
        </div>
        <div>
            <img class="image" src="<?php echo $GLOBALS["webRoot"]?>Resources/Public/Images/gruen.jpg"/>
            <div class="content-wrapper">
                <div class="text-wrapper">
                    <h1 class="display-1">420 nicht gefunden.</h1>
                    <h3>ups.</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <h1 class="display-1">Wie können wir helfen?</h1>
        <ul>
            <li><p>Prüfen Sie den aktuell aufgerufenen Link</p></li>
            <li><p>Wenn Sie glauben, das es sich hier um einen Irrtum handelt, <a href="<?php echo $GLOBALS["webRoot"]?>Kontakt">kontaktieren Sie uns.</a></p></li>
            <li><p>Hier finden Sie zurück auf unsere <a href="<?php echo $GLOBALS["webRoot"]?>">Startseite</a></p></li>
        </ul>
    </div>
</div>