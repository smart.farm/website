<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 07.05.2019
 * Time: 22:03
 */
use SmartFarm\Web\Classes\Search;

$results = [];

if(isset($_REQUEST['q']) && $_REQUEST['q'] != '') {
    $search = new Search(trim($_REQUEST['q'], " "));
    $search->execute();
    $results = $search->getResult();
}
?>

<div class="section mb-5">
    <div class="container">
        <h1 class="display-1">Suche</h1>
        <div class="list-group" id="list-tab" role="tablist">
            <?php
                if(!isset($_REQUEST['q']) || $_REQUEST['q'] === '') {
                    echo '<h3>Leider kann ich nicht sehen, wonach gesucht werden soll.</h3><p>Falls dieses weiterhin Problem besteht, bitte an den Systemadministrator wenden.</p>';
                }

                if(isset($_REQUEST['q']) && $_REQUEST['q'] != '') {
                    echo '<h3>Suchergebnisse für Suche nach "'.urlencode(trim($_REQUEST['q'], " ")).'":</h3>';

                    if(sizeof($results) === 0) {
                        echo '<h4>Diese Suche ergab leider keine Treffer.</h4>';
                    } else {
                        echo '<h4>Diese Suche ergab '.sizeof($results).' Treffer.</h4>';
                    }
                }

                foreach ($results as $key => $result) {

                    $title = $result['page']['title'];
                    if(isset($result['app']['title'])) {
                        $title = $result['app']['title'];
                    }

                    $teaser = 'Hier klicken um die Seite zu öffnen';
                    if(isset($result['app']['teaser']) && $result['app']['teaser'] != '') {
                        $teaser = $result['app']['teaser'];
                    }

                    $template = ' <a href="{{link}}" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="d-flex w-100 justify-content-between">
                                      <h5 class="mb-1">{{title}}</h5>
                                      <small class="text-muted">Relevanz: {{rank}}</small>
                                    </div>
                                    <p class="mb-1">{{teaser}}</p>
                                    <small class="text-muted">{{occurences}} Treffer</small>
                                  </a>';
                    $template = str_replace('{{link}}', $GLOBALS['webRoot'] . $result['link'], $template);
                    $template = str_replace('{{title}}', $title, $template);
                    $template = str_replace('{{rank}}', $result['rank'], $template);
                    $template = str_replace('{{occurences}}', $result['occurences'], $template);
                    $template = str_replace('{{teaser}}', $teaser, $template);

                    echo $template;
                }
            ?>
        </div>
    </div>
</div>
