<?php
/**
 * Created by PhpStorm.
 * User: Jan Storm
 * Date: 26.03.2019
 * The Home Page
 */
?>

<div class="section">
    <div class="feature-wrapper">
        <div class="container mb-4 mt-4">
            <div class="row">
                <div class="col-4 text-center">
                    <div class="img-wrapper mt-3 mb-3">
                        <img src="<?php echo $GLOBALS['webRoot']; ?>Resources/Public/Images/Startseite/feature-fast.png"/>
                    </div>
                    <div class="content">
                        <h3>Schnell</h3>
                        <p>Durch die zentrale Steuerung aller Geräte aus einer Hand, wird ein schneller und effizienter
                            Umgang garantiert.</p>
                    </div>
                </div>
                <div class="col-4 text-center">
                    <div class="img-wrapper mt-3 mb-3">
                        <img src="<?php echo $GLOBALS['webRoot']; ?>Resources/Public/Images/Startseite/feature-safe.png"/>
                    </div>
                    <div class="content">
                        <h3>Sicher</h3>
                        <p>Insgesamt sich auch eine Software zu verlassen ist vor allem eines: Sicher. Sämtliche
                            bisherige Sicherheitslücken minimieren sich auf ein Programm.</p>
                    </div>
                </div>
                <div class="col-4 text-center">
                    <div class="img-wrapper mt-3 mb-3">
                        <img src="<?php echo $GLOBALS['webRoot']; ?>Resources/Public/Images/Startseite/feature-aa.png"/>
                    </div>
                    <div class="content">
                        <h3>Qualitativ</h3>
                        <p>Durch ständige Kontrollen der Hard- sowie Software können wir einen hohen Qualitätsstandard
                            bieten.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="uerberuns" class="aboutus aos-wrapper">
        <div class="container" data-aos="fade-left">
            <h2>Wer sind wir?</h2>
            <p>Die Firma <strong>Smart</strong>Farm vertreibt ein System, welches Landwirten die Möglichkeit bietet,
                die für ihn essentiellen Gebäude/Gerätschaften &amp; Agrarflächen größtenteils vollautomatisch steuern
                und
                betreiben zu können.</p>
        </div>
        <div class="container">
            <h2>Unser Team</h2>
            <div class="row text-center">
                <div class="col-md-2"></div>
                <div class="col-md-4" data-aos="fade-up-right">
                    <div class="author">
                        <img class="author" height="80px" width="80px"
                             src="<?php echo $GLOBALS['webRoot']; ?>Resources/Public/Icons/Team/awilhelm.png"/>
                        <h3>Arne Wilhelm</h3>
                        <p>Geschäftsführer der SmartFarm GmbH. Koordiniert Vertieb & Finanzen</p>
                    </div>
                </div>
                <div class="col-md-4" data-aos="fade-up-left">
                    <div class="author">
                        <img class="author" height="80px" width="80px"
                             src="<?php echo $GLOBALS['webRoot']; ?>Resources/Public/Icons/Team/fkupfer.png"/>
                        <h3>Friedemann Kupfer</h3>
                        <p>Geschäftsführer der SmartFarm GmbH. Koordiniert Marketing & Personalverwalter </p>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row text-center">
                <div class="col-md-4" data-aos="fade-up-right">
                    <div class="author">
                        <img class="author" height="80px" width="80px"
                             src="<?php echo $GLOBALS['webRoot']; ?>Resources/Public/Icons/Team/mcroon.png"/>
                        <h3>Max Croon</h3>
                        <p>Entwickler der SmartFarm. Leiter der Steuerungssoftware-Entwicklung & Systeminfrastruktur - entwickelt ebenfalls in dem App-Team</p>
                    </div>
                </div>
                <div class="col-md-4" data-aos="fade-up">
                    <div class="author">
                        <img class="author" height="80px" width="80px"
                             src="<?php echo $GLOBALS['webRoot']; ?>Resources/Public/Icons/Team/mkrawtzow.png"/>
                        <h3>Martin Krawtzow</h3>
                        <p>Mediengestalter bei der SmartFarm. Leitet das Corporate Design, ist für Printmedien etc. Zuständig. Leitet das Qualitätsmanagement.</p>
                    </div>
                </div>
                <div class="col-md-4" data-aos="fade-up-left">
                    <div class="author">
                        <img class="author" height="80px" width="80px"
                             src="<?php echo $GLOBALS['webRoot']; ?>Resources/Public/Icons/Team/jstorm.png"/>
                        <h3>Jan Storm</h3>
                        <p>Entwickler der SmartFarm. Programmiert im Team der Steuerungssoftware, ist für die
                            Appentwicklung und die Webseite zuständig.</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="container" data-aos="fade-left">
            <h2>Unser Angebot</h2>
            <p>Dazu wird es eine Smarthome ähnliche einheitliche Oberfläche geben, die in
                der Basisversion (bestehend aus vollautomatischer Grundnahrungsversorgung) ausgeliefert wird und
                durch zusätzliche Module (in Eigen- bzw. Fremdentwicklung möglich) erweitert werden kann.</p>
        </div>

        <div class="container mt-4" data-aos="fade-left">
            <h2>Unsere Leistungen</h2>
            <p>Durch unser Produkt hat der Landwirt den Vorteil, alle notwendigen Einstellungen und Bedürfnisse auf
                einem Portal zentral verwalten zu können. Wir agieren als Schnittstelle (HUB), die unter anderem
                den Vorteil bietet, vollautomatisch eingesetzte Geräte von Herstellern untereinander zu verknüpfen.
                Hier profitiert der Kunde besonders von dem Prinzip, dass auch bereits vorhandene Gerätschaften in
                das neue System integriert werden können. Allgemein ergibt sich ein großer finanzieller Vorteil, da
                unser Produkt die zentrale Schnittstelle bildet und man nicht mehr einzelne teure Maschinen bei ein
                und demselben Hersteller erneut kaufen muss (Entfall von Einrichtungskosten, Verwaltungskosten,
                Wartungskosten etc.) sondern potentiell viele verschiedene Anbieter vergleichen und mit diesen
                unser System erweitern kann. Des weiteren wird der Betrieb in seiner Entscheidungsfindung beim
                Kauf von vollautomatischen landwirtschaftlichen Geräten (Hersteller, Preiskategorie, persönliche
                Anforderungen) sehr transparent und kann sich dem vollen Leistungsangebot des vorhandenen
                Marktes bedienen.</p>
        </div>
    </div>
    <?php
    if (!$GLOBALS['isMobile']) { // we dont want mobile users to waste their cellular data on this video
        echo '<div class="bg-movie-wrapper">';
        //         Add volume class to add volume
        echo '    <video width="320" height="240" class="bg-movie" controls>';
        echo '        <source src="' . $GLOBALS["webRoot"] . 'Resources/Public/Videos/Agrarlandschaften der Zukunft - 720p.mp4"';
        echo '                type="video/mp4">';
        echo '                    Your browser does not support the video tag.';
        echo '    </video>';
        echo '</div>';
    }
    ?>
</div>